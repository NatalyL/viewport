/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import com.sun.opengl.util.GLUT;
import static org.nlopez.FiguraViewPort.gl;

/**
 *
 * @author NATALY MICHELLE
 */
public class Figura {

    public void Dibujar(GLUT glut) {

        gl.glColor3f(0.8f, 0.4f, 0.8f);
        //Se dibuja un cono
        gl.glPushMatrix();
        gl.glScalef(1.5f, 1.5f, 1);
        gl.glRotatef(45, 0, 0, 1);
        glut.glutWireCone(1, 2, 4, 20);
        gl.glEnd();
        gl.glPopMatrix();

        //Se dibuja un cubo
        gl.glPushMatrix();
        gl.glTranslatef(0, 0, -1f);
        glut.glutWireCube(2f);
        gl.glEnd();
        gl.glPopMatrix();
    }
}
