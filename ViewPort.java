/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

/**
 *
 * @author NATALY MICHELLE
 */
public class ViewPort extends JFrame implements KeyListener {

    static GL gl;
    static GLU glu;
    static GLUT glut;
    static GLCanvas canvas;

    //Variables para extraer el ancho y alto de la ventana
    static int ancho, alto;

    //Variables para la rotacion
    private static float rotarX = 0;
    private static float rotarY = 0;
    private static float rotarZ = 0;

    //Variables para la traslacion
    private static float trasladaX = 0;
    private static float trasladaY = 0;
    private static float trasladaZ = 0;

    public static void main(String[] args) {

        ViewPort myframe = new ViewPort();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public ViewPort() {

        setSize(700, 600);
        setLocationRelativeTo(null);
        setTitle("Muchas figuras con ViewPort");
        setResizable(false);
        GraphicListener listener = new GraphicListener();
        alto = this.getHeight();
        ancho = this.getWidth();

        //Se crea el objeto de la clase canvas
        canvas = new GLCanvas();
        gl = canvas.getGL();
        glu = new GLU();
        glut = new GLUT();

        /*
         A�adimos el oyente de eventos para el renderizado de OPENGL,
         esto automaticamente llamara a init() y renderiza los graficos cuyo codigo
         haya sido escrito dentro del metodo dysplay
         */
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);

        Animator animator = new Animator(canvas);
        animator.start();
        addKeyListener(this);

    }

    public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {

            gl = arg0.getGL();
            gl.glClear(gl.GL_COLOR_BUFFER_BIT);
            gl.glColor3f(0.92f, 0.625f, 0.12f);

            gl.glMatrixMode(gl.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);//(-5.0, 30.0, -5.0, 20.0, -5.0, 10.0)

            //Se dibuja un cilindro
            gl.glViewport(0, (alto / 2) + 115, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(3, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            glut.glutWireCylinder(2, 30, 50, 50);

            //Se dibuja un esfera
            gl.glViewport(0, (alto / 2) - 30, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(1, 2, 1);//escalamos la figura
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(0.12f, 0.625f, 0.92f);
            glut.glutWireSphere(3, 10, 10);

            //Se dibuja un cono
            gl.glViewport(0, (alto / 4) - 20, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(3, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(0.12f, 0.92f, 0.625f);
            glut.glutWireCone(1, 2, 4, 20);

            //Se dibuja un dodecaedro
            gl.glViewport(0, (alto / 2) - 300, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(7, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(0.625f, 0.12f, 0.92f);
            glut.glutWireDodecahedron();

            //Se dibuja un isocaedro
            gl.glViewport(180, (alto / 2) + 115, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(1, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            glut.glutWireIcosahedron();

            //Se dibuja un ocathedron
            gl.glViewport(180, (alto / 2) - 10, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(2, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(0.12f, 0.165f, 0.92f);
            glut.glutWireSphere(2, 10, 10);
            glut.glutWireOctahedron();

//            Se dibuja un rombododecahedro
            gl.glViewport(180, (alto / 4) - 15, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(2, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            glut.glutWireRhombicDodecahedron();

            //Se dibuja un cubo
            gl.glViewport(180, (alto / 4) - 125, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(1, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            glut.glutWireCube(2f);

            //Se dibuja una esfera solida
            gl.glViewport(350, (alto / 2) + 105, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(1, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            glut.glutWireSphere(2, 50, 20);

            //Se dibuja una tetera
            gl.glViewport(350, (alto / 2) - 20, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(1, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(0.12f, 0.625f, 0.92f);
            glut.glutWireTeapot(2);

            //Se dibuja una tetraedro
            gl.glViewport(350, (alto / 2) - 170, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(1, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(0.12f, 0.92f, 0.625f);
            glut.glutWireTetrahedron();

            //Se dibuja torus
            gl.glViewport(350, (alto / 2) - 280, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glScalef(1, 2, 1);
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            gl.glColor3f(0.625f, 0.12f, 0.92f);
            glut.glutWireTorus(.5f, 1, 5, 40);

            gl.glFlush();
        }

        public void init(GLAutoDrawable drawable) {

        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {

        }
    }

    public void keyTyped(KeyEvent ke) {

    }

    //Creacion del metodo que detecta las teclas pulsadas
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            trasladaX += .1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            trasladaX -= .1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_UP) {
            trasladaY += .1f;
            System.out.println("Valor en la traslacion de Y: " + trasladaY);
        }

        if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            trasladaY -= .1f;
            System.out.println("Valor en la traslacion de X: " + trasladaY);
        }

        if (ke.getKeyCode() == KeyEvent.VK_U) {
            rotarX += 2.5f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_J) {
            rotarX -= 2.5f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_K) {
            rotarY += 2.5f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }

        if (ke.getKeyCode() == KeyEvent.VK_H) {
            rotarY -= 2.5f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }

        if (ke.getKeyCode() == KeyEvent.VK_N) {
            rotarZ += 1.0f;
            System.out.println("Valor en la rotacion en Z: " + rotarZ);
        }

        if (ke.getKeyCode() == KeyEvent.VK_M) {
            rotarZ -= 1.0f;
            System.out.println("Valor en la rotacion en Z: " + rotarZ);
        }

        if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
            trasladaX = 0;
            trasladaY = 0;
            rotarX = 0;
            rotarY = 0;
            rotarZ = 0;
        }
    }

    public void keyReleased(KeyEvent ke) {

    }

}
