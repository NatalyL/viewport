# viewport

Se subieron tres clases:

1. ViewPort

    En esta clase se presentan las figuras predeterminadas de glut, en la que cada una posee un espacio en la pantalla limitada por un view port, 
    en la cual cada figura se mueve en los ejes X y Y, y rota en los ejes X, Y y Z, todo por medio de comandos del teclado.

2. Figura

    En esta clase se encuentra el diseño de dos figuras predeterminadas de glut, el cono y el cubo.

3. FiguraViewPort

    Esta clase llama a la clase Figura para presentan dos figuras en la pantalla, una pequeña en la parte superior izquierda y otra mas grande en el 
    centro, en la que cada una posee un espacio en la pantalla limitada por un view port, en la cual cada figura se mueve en los ejes X y Y, y rota en 
    los ejes X, Y y Z, todo por medio de comandos del teclado.