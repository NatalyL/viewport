/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author NATALY MICHELLE
 */
public class FiguraViewPort extends JFrame implements KeyListener {

    static GL gl;
    static GLU glu;
    static GLUT glut;
    static GLCanvas canvas;

    //Variables para extraer el ancho y alto de la ventana
    static int ancho, alto;

    //Variables para la rotacion
    private static float rotarX = 0;
    private static float rotarY = 0;
    private static float rotarZ = 0;

    //Variables para la traslacion
    private static float trasladaX = 0;
    private static float trasladaY = 0;
    private static float trasladaZ = 0;

    public static void main(String[] args) {

        FiguraViewPort myframe = new FiguraViewPort();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public FiguraViewPort() {

        setSize(700, 600);
        setLocationRelativeTo(null);
        setTitle("Figura con ViewPort");
        setResizable(false);
        GraphicListener listener = new GraphicListener();
        alto = this.getHeight();
        ancho = this.getWidth();

        //Se crea el objeto de la clase canvas
        canvas = new GLCanvas();
        gl = canvas.getGL();
        glu = new GLU();
        glut = new GLUT();

        /*
         A�adimos el oyente de eventos para el renderizado de OPENGL,
         esto automaticamente llamara a init() y renderiza los graficos cuyo codigo
         haya sido escrito dentro del metodo dysplay
         */
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        
        Animator animator = new Animator(canvas);
        animator.start();
        addKeyListener(this);

    }

    public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {

            gl = arg0.getGL();
            gl.glClear(gl.GL_COLOR_BUFFER_BIT);
//            gl.glEnable(GL.GL_DEPTH_TEST);
            gl.glColor3f(0.92f, 0.625f, 0.12f);

            gl.glMatrixMode(gl.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);

        Figura f = new Figura();

            //Se dibuja figura peque�a
            gl.glViewport(0, (alto / 4)+270, ancho / 4, alto / 4);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glTranslatef(trasladaX, trasladaY, trasladaZ);
            gl.glScalef(1, 2, 1);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            f.Dibujar(glut);
            
            
            //Se dibuja figura
            gl.glViewport(0, alto/2-300, ancho, alto/2+100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glRotatef(10, 0, 1, 1);
            gl.glTranslatef(0.5f, 0, 1.5f);
            gl.glTranslatef(0, 0.5f, 0);
            gl.glTranslatef(trasladaX, trasladaY, trasladaZ);
            gl.glScalef(1, 2, 1);
            gl.glRotatef(rotarX, 1f, 0f, 0f);
            gl.glRotatef(rotarY, 0f, 1f, 0f);
            gl.glRotatef(rotarZ, 0f, 0f, 1f);
            f.Dibujar(glut);
            
            gl.glFlush();
        }

        public void init(GLAutoDrawable drawable) {
//
//            GL gl = drawable.getGL();
//            gl.glEnable(GL.GL_BLEND);
//            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
//            gl.glEnable(GL.GL_DEPTH_TEST);
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {

        }
    }

    public void keyTyped(KeyEvent ke) {

    }

    //Creacion del metodo que detecta las teclas pulsadas
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            trasladaX += .1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            trasladaX -= .1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_UP) {
            trasladaY += .1f;
            System.out.println("Valor en la traslacion de Y: " + trasladaY);
        }

        if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            trasladaY -= .1f;
            System.out.println("Valor en la traslacion de Y: " + trasladaY);
        }

        if (ke.getKeyCode() == KeyEvent.VK_Z) {
            trasladaZ += 1.0f;
            System.out.println("Valor en la traslacion de Z: " + trasladaZ);
        }

        if (ke.getKeyCode() == KeyEvent.VK_X) {
            trasladaZ -= 1.0f;
            System.out.println("Valor en la traslacion de Z: " + trasladaZ);
        }
        
        if (ke.getKeyCode() == KeyEvent.VK_U) {
            rotarX += 2.5f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_J) {
            rotarX -= 2.5f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if (ke.getKeyCode() == KeyEvent.VK_K) {
            rotarY += 2.5f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }

        if (ke.getKeyCode() == KeyEvent.VK_H) {
            rotarY -= 2.5f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }
        
        if (ke.getKeyCode() == KeyEvent.VK_N) {
            rotarZ += 1.0f;
            System.out.println("Valor en la rotacion en Z: " + rotarZ);
        }

        if (ke.getKeyCode() == KeyEvent.VK_M) {
            rotarZ -= 1.0f;
            System.out.println("Valor en la rotacion en Z: " + rotarZ);
        }

        if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
            trasladaX = 0;
            trasladaY = 0;
            trasladaZ = 0;
            rotarX = 0;
            rotarY = 0;
            rotarZ = 0;
        }
    }

    public void keyReleased(KeyEvent ke) {

    }

}

